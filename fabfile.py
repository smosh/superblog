from fabric.api import local, lcd

def prepare_deployment(branch_name):
	local('python manage.py test SuperBlog')
	local('git add -p && git commit') 

def deploy():
	with lcd('./SuperBlog/')
		
		# With git
		local('git pull ./SuperBlogDev/')

		# With both
		local('python manage.py migrate chat')
		local('python manage.py test chat')
		local('python -m CGIHTTPServer 80')
